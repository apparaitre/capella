<?php

add_action( 'wp_enqueue_scripts', 'wpbootstrap_parent_theme_enqueue_styles' );

function wpbootstrap_parent_theme_enqueue_styles() {
    wp_enqueue_style( 'wpbootstrap-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'capella-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( 'wpbootstrap-style' )
    );

}
